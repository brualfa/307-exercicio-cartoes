package tech.master.lancamento;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;


@FeignClient(name = "cliente")
public interface ClienteClient {
	  
	  @GetMapping("/buscar/{id}")
	  public Optional<Cliente> buscar(@PathVariable int id);
}
