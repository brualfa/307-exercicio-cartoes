package tech.master.lancamento;

import java.util.Optional;

import org.springframework.stereotype.Component;

@Component
public class CartaoFallBack implements CartaoClient  {
	
	@Override
	public Optional<Cartao> buscar(String numero)
	{
		Cartao cartao = new Cartao();
		cartao.setAtivo(true);
		cartao.setNumero("1111111111111111111");
		return Optional.of(cartao);
	}

}
