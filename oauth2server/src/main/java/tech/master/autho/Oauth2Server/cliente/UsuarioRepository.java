package tech.master.autho.Oauth2Server.cliente;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, String>{
  public Optional<Usuario> findByCpf(String cpf);
}
