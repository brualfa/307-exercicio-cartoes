package tech.master.cartao;

import java.util.Optional;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface ClienteClient {
  @GetMapping
  public Iterable<Cliente> listar();

  @GetMapping("/buscar/{cpf}")
  public Optional<Cliente> buscar(@PathVariable String cpf);
}
