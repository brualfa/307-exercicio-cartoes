package tech.master.cartao;

import java.security.Principal;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class CartaoController {

  @Autowired
  private CartaoService cartaoService;

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Cartao criar(@Valid @RequestBody Cartao cartao, Principal principal) {
    cartao.setCpfCliente(principal.getName());
    return cartaoService.criar(cartao);
  }

  @PatchMapping("/{numero}/ativar")
  public void ativar(@PathVariable String numero) {
    Optional<Cartao> optional = cartaoService.buscar(numero);

    if (!optional.isPresent()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }

    cartaoService.ativar(numero);
  }

  @GetMapping("/buscar/{numero}")
  public Optional<Cartao> buscar(@PathVariable String numero) {
    Optional<Cartao> optional = cartaoService.buscar(numero);

    if (!optional.isPresent()) {
      throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
    return optional;
  }

}
