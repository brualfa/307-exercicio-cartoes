package tech.master.cliente;

import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {
  @Autowired
  private ClienteService clienteService;
  
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public Cliente criar(@Valid @RequestBody Usuario usuario) {
    logger.info("Criando usuário " + usuario.getCpf());
    return clienteService.criar(usuario);
  }

  @GetMapping("/buscar/{cpf}")
  public Optional<Cliente> buscar(@PathVariable String cpf) {
    Optional<Cliente> cliente = clienteService.buscarPorCpf(cpf);

    if (cliente.isPresent()) {
      return cliente;
    }
      
    return Optional.empty();
  }
}